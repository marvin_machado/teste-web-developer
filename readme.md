Teste da Leroy Merlin

Teste WebDeveloper
PHP, Laravel ‘n Stuff.

Objetivo
Uma aplicação web de gerenciamento de produtos onde é necessário importar arquivos .xlsx (Excel) para criar produtos.

Requisitos
RUD (crud sem create) de produtos.
Tela para importar planilha .xlsx contendo uma lista de produtos.
Processar planilha (inserir/atualizar produtos) no background (queue).
OBS: A categoria dos produtos é uma string simples.

Requisitos Técnicos
Git
PHP
Laravel
Teste (PHPUnity)
Mockery
Queue
Modelo de planilha
https://dl.dropboxusercontent.com/u/12506137/tmp/products_teste_webdev_leroy.xlsx