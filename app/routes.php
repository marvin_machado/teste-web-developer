<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::action('FileController@create');
});

//File
Route::get( 'file',       'FileController@create');
Route::post('file/store', 'FileController@store');

//Product
Route::get(   'product',           'ProductController@index');
Route::get(   'product/{id}/edit', 'ProductController@edit');
Route::put(   'product/{id}',      'ProductController@update');
Route::delete('product/{id}',      'ProductController@destroy' );
