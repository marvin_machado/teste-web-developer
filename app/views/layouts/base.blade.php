<!DOCTYPE html>
<html class="no-js">

    <head>
        <title>Test WebDeveloper</title>
        <!-- Bootstrap -->
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ URL::asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ URL::asset('vendors/easypiechart/jquery.easy-pie-chart.css') }}" rel="stylesheet" media="screen">
        <link href="{{ URL::asset('css/styles.css') }}" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Leroy Merlin</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> Administrator <i class="caret"></i>

                                </a>
                            </li>
                        </ul>
                        @include('layouts.menu')

                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                @yield('sidebar')

                <!--/span-->
                <div class="span12" id="content">
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Arquivos</div>

                                </div>
                            </div>
                            <div class="block-content collapse in">
                                @yield('content')
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Test Webdeveloper 2015</p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="{{ URL::asset('vendors/jquery-1.9.1.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('vendors/easypiechart/jquery.easy-pie-chart.js') }}"></script>
        <script src="{{ URL::asset('js/testwebdeveloper.js') }}"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>