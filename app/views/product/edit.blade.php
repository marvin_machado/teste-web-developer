@section('content')

    {{ Form::model($product, ['action' => ['ProductController@update', $product->id], 'method' => 'PUT']) }}

        {{ Form::label('id', 'LM:') }}
        {{ Form::text('id') }}
        <br />

        {{ Form::label('name', 'Nome:') }}
        {{ Form::text('name') }}
        <br />

        {{ Form::label('free_shipping', 'Frete Grátis') }}
        {{ Form::select('free_shipping', array(0 => 'Não', 1 => 'Sim')) }}
        <br />

        {{ Form::label('description', 'Descrição:') }}
        {{ Form::text('description') }}
        <br />

        {{ Form::label('price', 'Preço:') }}
        {{ Form::text('price') }}
        <br />

        {{ Form::label('category', 'Categoria:') }}
        {{ Form::text('category') }}
        <br />

        {{ Form::button('Gravar', ['type' => 'submit']) }}

    {{ Form::close() }}

@stop
