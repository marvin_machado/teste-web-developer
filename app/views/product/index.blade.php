@section('content')
    <div class="span12">
        <!-- block -->
        <div class="block">
            <div class="block-content collapse in">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>LM</th>
                            <th>Nome</th>
                            <th>Frete Grátis</th>
                            <th>Descrição</th>
                            <th>Preço</th>
                            <th>Categoria</th>
                            <th>Editar</th>
                            <th>Excluir</th>
                        </tr>

                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <th> {{ $product->id }} </th>
                                <th> {{ $product->name }} </th>
                                @if ($product->free_shipping)
                                    <th> Sim </th>
                                @else
                                    <th> Não </th>
                                @endif
                                <th> {{ $product->description }} </th>
                                <th> {{ $product->price }} </th>
                                <th> {{ $product->category }} </th>
                                <th> <a class="btn btn-warning btn-mini" href="{{ URL::to('product/' . $product->id . '/edit') }}">Editar</a> </th>
                                <th>
                                    {{ Form::open(array('url' => 'product/' . $product->id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Excluir', array('class' => 'btn btn-danger btn-mini')) }}
                                    {{ Form::close() }}
                                </th>
                            </tr>
                        @endforeach

                     </thead>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
    </div>

@stop