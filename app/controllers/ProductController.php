<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $products = Product::all();

        $this->layout->content = View::make('product.index')
            ->with('products', $products);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $product = Product::where('id', '=', $id)->first();

        $this->layout->content = View::make('product.edit')
            ->with('product', $product);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $product = Product::find($id);

        $product->name          = Input::get('name');
        $product->free_shipping = Input::get('free_shipping');
        $product->description   = Input::get('description');
        $product->price         = Input::get('price');
        $product->category      = Input::get('category');

        if( $product->save() )
            return Redirect::action( 'ProductController@index' );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $product = Product::find($id);

        $product->delete();

        return Redirect::action( 'ProductController@index' );
	}


}
