<?php

class FileController extends BaseController
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->layout->content = View::make('file.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $file = new file();

        $file->upload(Input::file('file_upload'));

        Queue::push(function ($job) use ($file) {

            $file->fileImportProcess();

            $job->delete();

        });

        return Redirect::to('file');
    }

}
