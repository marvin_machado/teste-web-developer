<?php

use Mockery as m;

class FileControllerTest extends ControllerTestCase{

    public function testShouldCreate()
    {
        $this->requestAction('GET', 'FileController@create');
        $this->assertRequestOk();

    }

    public function testShouldStore()
    {
        $this->requestAction('POST', 'FileController@store');

        $file = m::mock('File');
        $file->shouldReceive('upload')->with('products_teste_webdev_leroy.xlxs', Mockery::any())->andReturn('true');
        $file->shouldReceive('fileImportProcess')->once()->andReturn('true');

        $this->assertRedirection(URL::action('FileController@index'));
    }
}