<?php

use Mockery as m;

class ProductControllerTest extends ControllerTestCase {

    public function testShouldIndex()
    {
        $this->requestAction('GET', 'ProductController@index');
        $this->assertRequestOk();
    }

    public function testShouldEdit()
    {
        $product = m::mock('Product');

        $this->requestAction('GET', 'Admin\CommentsController@edit', ['id'=>$product->id]);

        $this->assertRequestOk();

    }

    public function testShouldUpdate()
    {
        $product = m::mock('Product');

        $input = [
            'id' => 1,
            'name'=> 'Luva x',
            'free_shipping'=> 0,
            'description' => 'Luva x legal',
            'price' => 100.0,
            'category'=> 'Ferramentas'
        ];

        $this->withInput($input)->requestAction('PUT', 'ProductController@update', ['id'=>$product->id]);

        $this->assertRedirection(URL::action('ProductController@index'));
    }

    public function testShouldDestroy()
    {
        $product = m::mock('Product');

        $this->requestAction('DELETE', 'ProductController@destroy', array('id'=>$product->id));

        $this->assertRedirection( URL::action('ProductController@index') );

    }

}