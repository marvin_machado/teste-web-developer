<?php

class File
{

    protected $destinationPath;
    protected $fileName;

    public function upload($file)
    {

        $this->fileName = $file->getClientOriginalName();

        $this->destinationPath = str_replace('/', '\\', app_path('storage/uploads'));

        $temp = explode(".", $this->fileName);

        $extension = end($temp);

        $this->fileName = $temp[0] . '_' . date('YmdHis') . '.' . $extension;

        if ($file->move($this->destinationPath, $this->fileName)){
            return true;
        } else {
            return false;
        }

    }

    public function fileImportProcess()
    {

        $reader = Excel::selectSheetsByIndex(0)->load(storage_path('uploads\\' . $this->fileName))->toArray();

        $count = count($reader);

        for ($i = 3; $i < $count; $i++) {

            $product = Product::find($reader[$i][0]);

            if ( empty($product) ){
                $product = new Product();
            }


            $product->id = $reader[$i][0];
            $product->name = $reader[$i][1];
            $product->free_shipping = $reader[$i][2];
            $product->description = $reader[$i][3];
            $product->price = $reader[$i][4];
            $product->category = $reader[0][1];
            $product->save();

        }

        return unlink($this->destinationPath.'\\'.$this->fileName);

    }


}